#include <winsock2.h>
#include <stdio.h>
#include <stdlib.h>
#include <windows.h>
#include <curl/curl.h>


#define DEFAULT_PORT        5150
#define AZAMIUZUNLUK 1024

int g_port = DEFAULT_PORT;                  // Gelen Istekleri Dinleyecek Port
char g_szAddress[128];                      // Gelen Istekleri Dinleyecek Arayuz

DWORD WINAPI ClientThread(LPVOID lpParam)   //Dword = 32 bit isaretsiz tamsayi.
{
   SOCKET sock =(SOCKET)lpParam;
   int  ret;
   char ch;
   char str[AZAMIUZUNLUK];
   for (;;) {
      ret = recv(sock, &str, 1, 0);          //recv(socket,xxxxx, uzunluk, bayrak);
      if (ret == 0)
         break;
      if (ret == SOCKET_ERROR) {
         fprintf(stderr, "recv() failed: %d\n", WSAGetLastError());
         break;
      }
      if (str == '\x1b') {
            break;
      } else {
            puts(str);

      }

   }
   return 0;
}

int main(void)
{
   WSADATA wsd;
   SOCKET sListen,sClient;
   int addrSize;
   HANDLE hThread;
   DWORD dwThreadId;
   struct sockaddr_in local, client;

   if (WSAStartup(MAKEWORD(2,2), &wsd) != 0) {
      fprintf(stderr, "Winsock yuklemesi basarisiz!\n");
      return 1;
   } else {
      fprintf(stderr, "Winsock Yuklendi!\n");
   }


   sListen = socket(AF_INET, SOCK_STREAM, IPPROTO_IP);
   if (sListen == SOCKET_ERROR) {
      fprintf(stderr, "Socket Baglantisi Basarisiz!\n", WSAGetLastError());
      return 1;
   } else {
      fprintf(stderr, "Socket Baglantisi Basarili!\n", WSAGetLastError());
   }


   local.sin_addr.s_addr = htonl(INADDR_ANY);   // ip adresimi kullan
   local.sin_family = AF_INET;                  // adres ailesi Arpa Internet protokolu
   local.sin_port = htons(g_port);              // default port numarasi

   if (bind(sListen, (struct sockaddr *)&local,
         sizeof(local)) == SOCKET_ERROR) {
      fprintf(stderr, "bind() failed: %d\n", WSAGetLastError());
      return 1;
   }
   listen(sListen, 8);

   for (;;) {
      addrSize = sizeof(client);
      sClient = accept(sListen, (struct sockaddr *) &client, &addrSize);
      if (sClient == INVALID_SOCKET) {
         fprintf(stderr, "accept() failed: %d\n", WSAGetLastError());
         break;
      }
      fprintf(stderr, "Baglanan istemci: %s:%d\n", inet_ntoa(client.sin_addr), ntohs(client.sin_port));

      hThread = CreateThread(NULL, 0, ClientThread, (LPVOID)sClient, 0, &dwThreadId);
      if (hThread == NULL) {
         fprintf(stderr, "CreateThread() failed: %d\n", GetLastError());
         break;
      }
      CloseHandle(hThread);
   }
   closesocket(sListen);

   WSACleanup();

   return 0;
}
